import { expect, tap } from '@pushrocks/tapbundle';
import * as smartfuzzy from '../ts/index';

tap.test('should sort objects', async () => {
  class Car {
    constructor(public brand: string) {}
  }

  let testObjectSorter: smartfuzzy.ObjectSorter<Car>;

  testObjectSorter = new smartfuzzy.ObjectSorter([
    new Car('BMW'),
    new Car('Mercedes Benz'),
    new Car('Volvo'),
  ]);

  const result = testObjectSorter.sort('Volvo', ['brand']);
  console.log(result);
});

tap.start();
